import React from "react";
import HeaderComponent from "../../components/Header/HeaderComponent";
import DonationContainer from "./DonationContainer/DonationContainer";
import "./DonationPage.css";

const DonationPage = () => {
  return (
    <div className="donationView">
      <HeaderComponent />
      <DonationContainer />
    </div>
  );
};

export default DonationPage;
