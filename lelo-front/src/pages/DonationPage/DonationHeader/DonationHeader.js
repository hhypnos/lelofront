import React from "react";
import "./DonationHeader.css";
import logo from "../../../images/lelologo.svg";
import menu from "../../../images/component.svg";
const DonationHeader = () => {
  return (
    <div className="header">
      <div>
        <img src={logo} alt="logo" className="header-logo" />
      </div>
      <div>
        <div className="header-wrapper">
          <img src={menu} alt="menu" className="menu-icon" />

          <a href="/" className="menu-item active">
            მთავარი
          </a>
          <a href="/" className="menu-item">
            კონტაქტი
          </a>
          <a href="/" className="menu-item">
            ჩვენს შესახებ
          </a>
        </div>
      </div>
      <div>
        <button className="donation-btn">დონაცია</button>
      </div>
    </div>
  );
};

export default DonationHeader;
