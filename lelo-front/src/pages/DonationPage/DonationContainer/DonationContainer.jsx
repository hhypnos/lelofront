import React from "react";
import "./DonationContainer.css";

const DonationContainer = () => {
  const prices = [10, 20, 50, 100];
  return (
    <div className="donation-container">
      <div className="donation-left">
        <h2 className="donationText">
          თქვენ შეგიძლიათ განახორციელოთ დონაცია „ლელოსთვის“!
        </h2>
        <p>
          თქვენი მხარდაჭერა და წვლილი მნიშვნელოვანია. გადარიცხვა შესაძლებელია
          ჩვენი ვებგვერდი საშუალებითაც.ხოლო დიდი თანხის შემთხვევაში, გთხოვთ
          თანხა ჩარიცხოთ ინტერნეტბანკით
        </p>
        <h2 className="donation-rec">ლარის ანგარიშის რეკვიზიტები:</h2>
        <p>
          სს თიბისი ბანკი საბანკო კოდი - TBCBGE 22 მიმღების დასახელება -
          პოლიტიკური გაერთიანება ლელოსთვის ანგარიშსწორების ანგარიში -
          GE84TB7085936080100010
        </p>
      </div>
      <div className="donation-right">
        <div className="flex-end container-w">
          <div className="d-flex">
            <div className="filed-n">
              <label htmlFor="name">სახელი</label>
              <input type="text" id="name" name="name" className="name" />
            </div>
            <div className="filed-n">
              <label htmlFor="lastname">გვარი</label>
              <input
                type="text"
                id="lastname"
                name="lastname"
                className="surname"
              />
            </div>
            <div className="filed-n">
              <label htmlFor="pn">პირადი ნომერი</label>
              <input type="text" id="pn" name="pn" className="passport-n" />
            </div>
          </div>
          <div className="filed-n">
            <label htmlFor="email">ელ-ფოსტა</label>
            <input type="email" id="email" name="email" className="email-n" />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              padding: "5px",
            }}
          >
            <div className="">
              <label htmlFor="phone">ტელეფონი</label>
              <input type="text" id="phone" name="phone" />
            </div>
            <div className="filed-n">
              <label htmlFor="code">კოდი</label>
              <input type="text" id="code" name="code" className="code" />
            </div>
            <button className="send-btn">გაგზავნა</button>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              padding: "5px",
            }}
          >
            {prices.map((price) => (
              <div
                key={price}
                className={`price-card ${price === 50 && "active"}`}
                key={price}
              >
                {price}
              </div>
            ))}
          </div>
          <div className="filed-n">
            <input
              type="text"
              placeholder="ან მიუთითეთ თანხა"
              className="btn-don"
            />
          </div>
          <button className="donat-b">დონაცია</button>
        </div>
      </div>
    </div>
  );
};

export default DonationContainer;
