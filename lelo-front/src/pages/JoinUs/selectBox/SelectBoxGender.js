import React from "react";
import "./styles.css";
import logo from "../../../images/icon.svg";

function DropDown({ selected, setSelected, optionsGender }) {
  const [isActive, setIsActive] = React.useState(false);
  const options = optionsGender;
  return (
    <div className="dropdown" onClick={(e) => setIsActive(!isActive)}>
      <div className="dropdown-btn">{selected}</div>
      <span className="fas fa-caret-down"></span>
      {isActive && (
        <div className="dropdown-content">
          {options.map((option) => {
            return (
              <div
                key={option}
                className="dropdown-item"
                onClick={(e) => {
                  setSelected(option);
                }}
              >
                <p className="item">{option}</p>
              </div>
            );
          })}
        </div>
      )}
      <img src={logo} className="logo" />
    </div>
  );
}

export default DropDown;
