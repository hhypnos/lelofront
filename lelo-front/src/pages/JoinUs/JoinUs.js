import React from "react";
import HeaderComponent from "../../components/Header/HeaderComponent";
import MemberUser from "../../components/Member/MemberUser";
import SupporterUser from "../../components/Suporter/SuporterUser";
import "./JoinUs.css";
const JoinUs = () => {
  const [state, setState] = React.useState(true);
  const [color, setColor] = React.useState("red");

  const handleClick = () => {
    setState(!state);
    setColor(state ? "red" : "white");
  };
  return (
    <div className="JoinUswrapper">
      <HeaderComponent />
      <div className="toggleView">
        <p
          className={state ? "buttonTrue" : "buttonFalse"}
          onClick={handleClick}
        >
          გახდი წევრი
        </p>
        <p
          className={state ? "buttonFalse" : "buttonTrue"}
          onClick={handleClick}
        >
          გახდი მხარდამჭერი
        </p>
      </div>
      {state ? <MemberUser /> : <SupporterUser />}
    </div>
  );
};

export default JoinUs;
