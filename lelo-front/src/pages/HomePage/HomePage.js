import React from "react";
import HeaderComponent from "../../components/Header/HeaderComponent";
import Slider from "../../components/Slider/Slider";
import CardComponent from "../../components/card/CardComponent";
import Donation from "../../components/Donation/Donation";
import OurTeam from "../../components/OurTeam/OurTeam";
import Contact from "../../components/Contact/Contact";

const HomePage = () => {
  return (
    <div>
      <HeaderComponent isAbsolute={true}/>

      <Slider />
      <OurTeam />
      <Contact />
      {/* <Donation /> */}
    </div>
  );
};

export default HomePage;
