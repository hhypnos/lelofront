import React from 'react';
import HeaderComponent from '../../components/Header/HeaderComponent';
import Contact from '../../components/Contact/Contact';
import  './ContactUs.css';

const ContactUs = () => {
    return (
        <div className="contactUs-wrap">
            <HeaderComponent />
            <Contact contact={true}/>
        </div>
    );
};

export default ContactUs;