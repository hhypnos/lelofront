import React from 'react';
import HeaderComponent from '../../components/Header/HeaderComponent';
import OurTeam from '../../components/OurTeam/OurTeam';

const OurTeamPage = () => {
    return (
        <div style={{ display: 'flex', flexDirection :'column',alignItems:'center'}}>
            <HeaderComponent />
            <OurTeam team={true}/>
        </div>
    );
};

export default OurTeamPage;