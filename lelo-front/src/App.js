import "./App.css";
import { Switch, Route } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import DonationPage from "./pages/DonationPage/DonationPage";
import Footer from "./components/Footer/Footer";
import JoinUs from "./pages/JoinUs/JoinUs";
import OurTeamPage from './pages/OurTeam/OurTeamPage';
import ContactUs from './pages/contact/ContactUs';
function App() {
  return (
    <>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/donation" component={DonationPage} />
        <Route path="/JoinUs" component={JoinUs} />
        <Route path="/OurTeam" component={OurTeamPage} />
        <Route path="/Contact" component={ContactUs} />
      </Switch>
      <Footer />
    </>
  );
}

export default App;
