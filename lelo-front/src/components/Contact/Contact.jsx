import React from "react";
import "./Contact.css";

const Contact = ({ contact }) => {
  return (
    <div className={!contact ? "contact-wrapper container" : "contact-page"}>
      <div className="contact-text">
        <h3>კონტაქტი</h3>
        <p>სამუშაო განრიგი: ორშაბათი - პარასკევი, 10:00 - 19:00</p>
        <p>თბილისი, ცენტრალი</p>
        <p>
          ტელეფონი <span>032 223 22 88</span>{" "}
        </p>
        <p>
          ელ.ფოსტა <span>info@lelo2020.com</span>
        </p>
        <div className="d-flex contact-form">
          <div className="form-group p-0">
            <label htmlFor="name">სახელი</label>
            <input type="text" id="name" name="name" style={{ width: 90 }} />
          </div>
          <div className="form-group p-0" style={{ marginLeft: 21 }}>
            <label htmlFor="lastname">გვარი</label>
            <input
              type="text"
              id="lastname"
              name="lastname"
              style={{ width: 93 }}
            />
          </div>
          <div className="form-group p-0" style={{ marginLeft: 21 }}>
            <label htmlFor="pn">ელ.ფოსტა</label>
            <input type="email" id="pn" name="pn" style={{ width: 141 }} />
          </div>
        </div>
        <div
          className="form-group p-0"
          style={{ width: "100%", marginTop: 21 }}
        >
          <label htmlFor="pn" style={{ marginBottom: 9 }}>
            ტექსტი
          </label>
          <textarea></textarea>
          <div className="send-contact">
            <div className="sent">გაგზავნა</div>
          </div>
        </div>
      </div>
      <div className="contact-map">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d26981.500467437276!2d42.31306904956082!3d41.854792854593526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ska!2sge!4v1630771530153!5m2!1ska!2sge"
          width="574"
          height="555"
          allowFullScreen=""
          loading="lazy"
        ></iframe>
      </div>
    </div>
  );
};

export default Contact;
