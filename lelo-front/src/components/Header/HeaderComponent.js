import React from 'react';
import './HeaderStyles.css';
import logo from "../../images/lelologo.svg";
import menu from "../../images/component.svg";
import { useHistory } from 'react-router-dom';

const HeaderComponent = ({isAbsolute}) => {
      const history = useHistory()

  return (
    <div className={`header ${isAbsolute && 'absolute'}`}>
      <div>
        <img src={logo} alt="logo" className="header-logo" />
      </div>
      <div>
        <div className="header-wrapper">
          <img src={menu} alt="menu" className="menu-icon" />

          <a href="/Contact" className="menu-item active">
            მთავარი
          </a>
          <div href="/" className="menu-item" onClick={()=> history.push('/Contact')}>
            კონტაქტი
          </div>
          <a href="/" className="menu-item">
            ჩვენს შესახებ
          </a>
        </div>
      </div>
      <div className="buttonWrapper">
          <button className="join" onClick={()=> history.push('/JoinUs')}>შემოგვიერთდი</button>
        <button className="donat-bt" onClick={()=> history.push('/donation')}>დონაცია</button>
      </div>
    </div>
  );
};


export default HeaderComponent