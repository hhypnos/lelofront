import React from 'react';
import { useHistory } from "react-router-dom";

const Donation = () => {
    const history = useHistory()
    return (
        <div onClick={()=> history.push('/donation')}>
            Donation
        </div>
    );
};

export default Donation;