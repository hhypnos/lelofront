import React from "react";
import { Swiper as ReactSwiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation } from "swiper/core";

import "swiper/swiper.min.css";

import MapImg from "../../images/map.svg";
import CoverImg from "../../images/cover.svg";
import Cover from "../../images/cover.jpeg";
import PersonImg from "../../images/person.png";
import shape from "../../images/shape.svg";
import "./Slider.css";

SwiperCore.use([Navigation]);

const SliderCard = ({ onClick, i, img, hasChild, checkSliderPosition }) => {
  return (
    <div className="c-box" onClick={onClick} key={i}>
      <img src={img} />
      <div className="c-box-textBlock">
        <h4 className="person-name">მამუკა ხაზარაძე {i + 1}</h4>
        <h5 className="person-position">თავმჯდომარე</h5>
      </div>
      {hasChild && (
        <div
          className={`c-box-after ${checkSliderPosition(i + 1)}`}
          onClick={onClick}
        >
          <img src={img} />
          <div>
            <h4 className="person-name">მამუკა ხაზარაძე </h4>
            <h5 className="person-position">თავმჯდომარე</h5>
            <p>
              ბიზნესმენი. ჰარვარდის ბიზნეს სკოლის კურსდამთავრებული. არის თიბისი
              ბანკის, „ბორჯომის“, „ანაკლიის განვითარების კონსორციუმის“, ლისი
              დეველოპმენტის, ამერიკული აკადემიის და სხვა მრავალი წარმატებული
              ორგანიზაციის დამფუძნებელი, პრეზიდენტი და საკოორდინაციო საბჭოს
              თავმჯდომარე.
            </p>
          </div>
        </div>
      )}
    </div>
  );
};

const SliderBigCard = ({ onClick, i, img, hasChild, checkSliderPosition }) => {
  return (
    <div
      className={`c-box--big ${i === 2 ? "black" : ""}`}
      onClick={onClick}
      key={i}
    >
      <img src={img} />
      <div className="c-box-textBlock">
        <div className="c-left">
          <h4 className="person-name">მამუკა ხაზარაძე</h4>
          <h5 className="person-position">თავმჯდომარე</h5>
        </div>
        <div className="c-right">
          <h3>მერობის კანდიდატი</h3>
        </div>
      </div>
      {hasChild && (
        <div
          className={`c-box--big-after ${checkSliderPosition(i + 1)}`}
          style={i === 1 ? { height: i === 1 ? "250px" : " 216px" } : null}
          onClick={onClick}
        >
          <img src={img} />
          <div>
            <div className="d-flex">
              <div>
                <h4 className="person-name">მამუკა ხაზარაძე</h4>
                <h5 className="person-position">თავმჯდომარე</h5>
              </div>
              <h3>მერობის კანდიდატი</h3>
            </div>
            <p>
              ბიზნესმენი. ჰარვარდის ბიზნეს სკოლის კურსდამთავრებული. არის თიბისი
              ბანკის, „ბორჯომის“, „ანაკლიის განვითარების კონსორციუმის“, ლისი
              დეველოპმენტის, ამერიკული აკადემიის და სხვა მრავალი წარმატებული
              ორგანიზაციის დამფუძნებელი, პრეზიდენტი და საკოორდინაციო საბჭოს
              თავმჯდომარე.
            </p>
          </div>
        </div>
      )}
    </div>
  );
};

const Slider = () => {
  const slides = [
    {
      id: 1,
      title: "გაეცანით მარშალის გეგმას",
      text: "თქვენი მხარდაჭერა და წვლილი მნიშვნელოვანია. გადარიცხვა შესაძლებელიაჩვენი ვებგვერდის საშუალებითაც. ხოლო დიდი თანხის შემთხვევაში, გთხოვთ თანხა ჩარიცხოთ ინტერნეტბანკით",
      image: Cover,
    },
    {
      id: 2,
      title: "მამუკა გვიშველე!",
      text: "თქვენი მხარდაჭერა და წვლილი მნიშვნელოვანია. გადარიცხვა შესაძლებელიაჩვენი ვებგვერდის საშუალებითაც. ხოლო დიდი თანხის შემთხვევაში, გთხოვთ თანხა ჩარიცხოთ ინტერნეტბანკით",
      image: CoverImg,
    },
  ];
  const [activeSlide, setActiveSlide] = React.useState(0);
  const [activeTopSlide, setActiveTopSlide] = React.useState(0);
  const [swiper, setSwiper] = React.useState(null);
  const [topSwiper, setTopSwiper] = React.useState(null);

  const [activeCard, setActiveCard] = React.useState(null);
  const topSlideTo = (index) => {
    if (topSwiper) topSwiper.slideTo(index);
  };
  const slideTo = (index) => {
    if (swiper) swiper.slideTo(index);
  };

  const switchSlide = (i) => {
    slideTo(i);
    setActiveSlide(i);
    setActiveCard(null);
  };

  const checkSliderPosition = (i) => {
    if (i === 6) return "c-box-after--rightBottom";
    else if (i === 3) return "c-box-after--right";
    else if (i === 4 || i === 5) return "c-box-after--bottom";
    else return "";
  };

  const checkOriginalSliderPosition = (i) => {
    if (i % 3 === 0 && i === 12) return "c-box-after--rightBottom";
    else if (i % 3 === 0) return "c-box-after--right";
    else if (i === 10 || i === 11) return "c-box-after--bottom";
    else return "";
  };

  const candidatesArray = [
    [[...Array(4)], [...Array(6)]],
    [[], [...Array(12)]],
  ];

  return (
    <div className="main-wrapper">
      <div className="slider-wrapper">
        <ReactSwiper
          slidesPerView={1}
          spaceBetween={0}
          onSlideChange={(swiper) => setActiveTopSlide(swiper.activeIndex)}
          onSwiper={setTopSwiper}
        >
          {slides.map((slide) => {
            return (
              <SwiperSlide key={slide.id}>
                <div className="main-container">
                  <img src={slide.image} width="100%" height="714" />
                  <div className="main-text">
                    <h3>{slide.title}</h3>
                    <p>{slide.text}</p>
                    <div className="dots-wrapper flex-start">
                      {slides.map((i, key) => (
                        <div
                          key={key}
                          onClick={() => topSlideTo(key)}
                          className={`dot ${
                            key === activeTopSlide && "active"
                          }`}
                        ></div>
                      ))}
                    </div>
                  </div>
                </div>
              </SwiperSlide>
            );
          })}
        </ReactSwiper>
        <img src={shape} className="overSlider" />
      </div>
      <div className="bg-wrapper">
        <div className="plan-container container">
          <div className="plan-text">
            <h3>გაეცანით მარშალის გეგმას</h3>
            <p>
              თქვენი მხარდაჭერა და წვლილი მნიშვნელოვანია. გადარიცხვა
              შესაძლებელია ჩვენი ვებგვერდის საშუალებითაც. ხოლო დიდი თანხის
              შემთხვევაში, გთხოვთ თანხა ჩარიცხოთ ინტერნეტბანკით
            </p>
          </div>
          <div className="plan-map">
            <img src={MapImg} />
          </div>
        </div>
        <div className="slider-wrapper">
          <div className="candidates container">
            <h3 className="c-title">კანდიდატები</h3>
            <ReactSwiper
              slidesPerView={1}
              spaceBetween={0}
              onSwiper={setSwiper}
              onSlideChange={(swiper) => switchSlide(swiper.activeIndex)}
            >
              {candidatesArray.map((cand, key) => {
                return (
                  <SwiperSlide key={key}>
                    <div className="big-cards-box" style={{ marginBottom: 25 }}>
                      {cand &&
                        cand[0] &&
                        cand[0].map((c, i) => {
                          return activeCard === i ? (
                            i % 2 == 0 ? (
                              <SliderBigCard
                                key={i}
                                img={PersonImg}
                                i={i}
                                onClick={() => setActiveCard(null)}
                                hasChild={true}
                                checkSliderPosition={checkSliderPosition}
                              />
                            ) : (
                              <SliderCard
                                key={i}
                                onClick={() => setActiveCard(null)}
                                i={i}
                                img={PersonImg}
                                hasChild={true}
                                checkSliderPosition={() =>
                                  checkSliderPosition(3)
                                }
                              />
                            )
                          ) : i % 2 == 0 ? (
                            <SliderBigCard
                              img={PersonImg}
                              i={i}
                              onClick={() => setActiveCard(i)}
                            />
                          ) : (
                            <SliderCard
                              onClick={() => setActiveCard(i)}
                              i={i}
                              checkSliderPosition={() => checkSliderPosition()}
                              img={PersonImg}
                            />
                          );
                        })}
                    </div>
                    <div className="c-flex">
                      {cand &&
                        cand[1] &&
                        cand[1].map((c, i) => {
                          return activeCard === i + cand[0].length ? (
                            <SliderCard
                              key={i}
                              onClick={() => setActiveCard(null)}
                              i={i}
                              img={PersonImg}
                              hasChild={true}
                              checkSliderPosition={
                                key == 0
                                  ? checkSliderPosition
                                  : checkOriginalSliderPosition
                              }
                            />
                          ) : (
                            <SliderCard
                              key={i}
                              onClick={() => setActiveCard(i + cand[0].length)}
                              i={i}
                              img={PersonImg}
                            />
                          );
                        })}
                    </div>
                  </SwiperSlide>
                );
              })}
            </ReactSwiper>
            <div className="dots-wrapper">
              {candidatesArray.map((c, i) => (
                <div
                  key={i}
                  onClick={() => switchSlide(i)}
                  className={`dot ${i === activeSlide && "active"}`}
                ></div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Slider;
