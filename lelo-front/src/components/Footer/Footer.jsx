import React from "react";
import "./Footer.css";
import logo from "../../images/lelologo.svg";

const Footer = () => {
  return (
    <footer>
      <div className="footer-container">
        <div className="d-flex">
          <div className="d-flex">
            <a href="#">კონტაქტი</a>
            <a href="#">კონტაქტი</a>
          </div>
          <img src={logo} alt="logo" className="header-logo" />
          <div className="d-flex">
            <a href="#">f</a>
            <a href="#">a</a>
            <a href="#">s</a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
