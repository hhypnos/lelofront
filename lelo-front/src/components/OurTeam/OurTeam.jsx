import React from "react";
import { Swiper as ReactSwiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation } from "swiper/core";
import { useHistory } from "react-router-dom";
import "swiper/swiper.min.css";
import "./OurTeam.css";

import PersonImg from "../../images/person.png";
import AboutImg from "../../images/about.png";

SwiperCore.use([Navigation]);

const OurTeam = ({ team }) => {
  const history = useHistory();

  const checkSliderPosition = (i) => {
    if (i % 3 === 0 && i === 12) return "c-box-after--rightBottom";
    else if (i % 3 === 0) return "c-box-after--right";
    else if (i === 10 || i === 11) return "c-box-after--bottom";
    else return "";
  };
  const [activeCard, setActiveCard] = React.useState(null);
  const [activeSlide, setActiveSlide] = React.useState(0);
  const [swiper, setSwiper] = React.useState(null);
  const slideTo = (index) => {
    if (swiper) swiper.slideTo(index);
  };
  const changeSlide = (i) => {
    slideTo(i);
    setActiveSlide(i);
    setActiveCard(null);
  };
  const candidatesArray = [[...Array(12)], [...Array(12)]];
  return (
    <div className={team ? "d-block" : "c-block"}>
      <div className="about-wrapper d-flex container">
        <img src={AboutImg} alt="about-us" title="about-us" />
        <div className="about-text-wrapper">
          <h3>ჩვენს შესახებ</h3>
          <h2>რა აერთიანებს "ლელოს"?</h2>
          <p>
            ჩვენ, პოლიტიკური გაერთიანება "ლელო საქართველოსთვის" ვერთიანდებით
            თავისუფლების, განვითარებისა და პატრიოტიზმის იდეალებით, რათა ქვეყანამ
            დაძლიოს მრავალმხრივი კრიზისი, დაადგეს აღმავლობის შეუქცევად გზას და
            დაიმკვიდროს ღირსეული ადგილი დემოკრატიული ქვეყნების თანამეგობრობაში
          </p>
          {team ? null : (
            <div onClick={() => history.push("/OurTeam")}>იხილეთ მეტი</div>
          )}
        </div>
      </div>
      <div className="container">
        <h3 className="c-title">ჩვენი გუნდი</h3>

        <ReactSwiper
          slidesPerView={1}
          spaceBetween={0}
          onSwiper={setSwiper}
          onSlideChange={(swiper) => changeSlide(swiper.activeIndex)}
        >
          {candidatesArray.map((cand, key) => {
            return (
              <SwiperSlide key={key}>
                <div className="c-flex">
                  {cand.map((a, i) => {
                    return activeCard === i ? (
                      <div className="c-box" key={i}>
                        <img src={PersonImg} />
                        <div>
                          <h4 className="person-name">მამუკა ხაზარაძე</h4>
                          <h5 className="person-position">თავმჯდომარე</h5>
                        </div>
                        <div
                          className={`c-box-after ${checkSliderPosition(
                            i + 1
                          )}`}
                          onClick={() => setActiveCard(null)}
                        >
                          <img src={PersonImg} />
                          <div>
                            <h4 className="person-name">მამუკა ხაზარაძე</h4>
                            <h5 className="person-position">თავმჯდომარე</h5>
                            <p>
                              ბიზნესმენი. ჰარვარდის ბიზნეს სკოლის
                              კურსდამთავრებული. არის თიბისი ბანკის, „ბორჯომის“,
                              „ანაკლიის განვითარების კონსორციუმის“, ლისი
                              დეველოპმენტის, ამერიკული აკადემიის და სხვა მრავალი
                              წარმატებული ორგანიზაციის დამფუძნებელი, პრეზიდენტი
                              და საკოორდინაციო საბჭოს თავმჯდომარე.
                            </p>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div
                        className="c-box"
                        onClick={() => setActiveCard(i)}
                        key={i}
                      >
                        <img src={PersonImg} />
                        <div className="c-box-textBlock">
                          <h4 className="person-name">მამუკა ხაზარაძე</h4>
                          <h5 className="person-position">თავმჯდომარე</h5>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </SwiperSlide>
            );
          })}
        </ReactSwiper>
        <div className="dots-wrapper">
          {candidatesArray.map((c, i) => (
            <div
              key={i}
              onClick={() => changeSlide(i)}
              className={`dot ${i === activeSlide && "active"}`}
            ></div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default OurTeam;
