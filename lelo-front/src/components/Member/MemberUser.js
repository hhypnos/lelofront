import React from "react";
import "./MemberUser.css";
import DropDownGender from "../../pages/JoinUs/selectBox/SelectBoxGender";
import DropDownAdress from "../../pages/JoinUs/selectBox/SelectBoxAdress";
import logo from "../../images/calendar.svg";

const MemberUser = () => {
  const [selectedGender, setSelectedGender] = React.useState("");
  const [selectedAdress, setSelectedAdress] = React.useState("");
  const [gender, setGender] = React.useState(["Male", "Female", "Other"]);
  const [adress, setAdress] = React.useState([
    "Tbilisi",
    "Kutaisi",
    "Zestafoni",
  ]);
  return (
    <div>
      <div className="donation-right">
        <div className="flex-end">
          <div className="d-flex gap22">
            <div className="form-group">
              <label htmlFor="name">სახელი</label>
              <input type="text" id="name" name="name" />
            </div>
            <div className="form-group">
              <label htmlFor="lastname">გვარი</label>
              <input type="text" id="lastname" name="lastname" />
            </div>
            <div className="form-group">
              <label htmlFor="pn">პირადი ნომერი</label>
              <input type="text" id="pn" name="pn" className="passport" />
            </div>
          </div>
          <div className="d-flex gap22">
            <div className="form-group">
              <label htmlFor="email">აირჩიეთ სქესი</label>
              <DropDownGender
                selected={selectedGender}
                setSelected={setSelectedGender}
                optionsGender={gender}
              />
            </div>
            <div className="form-group">
              <label htmlFor="email">ელ-ფოსტა</label>
              <input type="email" id="email" name="email" className="email" />
            </div>
          </div>
          <div className="d-flex gap19">
            <div className="form-group">
              <label htmlFor="phone">მობილური</label>
              <input type="text" id="phone" name="phone" className="mobile" />
            </div>
            <div className="form-group">
              <label htmlFor="code">ტელეფონი</label>
              <input
                type="text"
                id="telephone"
                name="telephone"
                className="telephone"
              />
            </div>
          </div>
          <div className="d-flex gap13">
            <div className="form-group" className="calendarView">
              <label htmlFor="code">დაბადების თარიღი</label>

              <input
                type="text"
                id="telephone"
                name="telephone"
                className="telephone"
              />
              <img src={logo} className="logoCalendar" />
            </div>
            <div className="form-group">
              <label htmlFor="email">აირჩიეთ ქალაქი</label>
              <DropDownAdress
                selected={selectedAdress}
                setSelected={setSelectedAdress}
                optionsAdress={adress}
              />
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="email">მისამართი</label>
            <input
              type="text"
              id="location"
              name="location"
              className="mobile"
            />
          </div>
          <button className="donate-btn">გაგზავნა</button>
          <div className="d-flex gap-13" style={{ justifyContent: "center" }}>
            <p className="description">
              ფორმის შევსების შემდეგ პარტია "ლელო საქართველოსთვის" იტოვებს
              უფლებას, გამოიყენოს თქენ მიერ მითითებული ინფორმაცია თქვენთან
              კონტაქტისთვის, ჩვენთვის გაზიარებული თქვენი პერსონალური მონაცემები
              შეინახება პარტიის ბაზაში და დაცული იქნება საქართველოს
              კანონმდებლობის შესაბამისად.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MemberUser;
